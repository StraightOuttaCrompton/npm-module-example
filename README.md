# Npm module example

Bare minimum example repository to automate npm publishing with gitlab ci.

Based on [this blog](https://vgarcia.dev/blog/2019-11-06--publish-a-package-to-npm-step-by-step/).

## Steps

### Install required packages

```sh
npm i -D @commitlint/cli @commitlint/config-conventional @semantic-release/changelog @semantic-release/commit-analyzer @semantic-release/git @semantic-release/gitlab @semantic-release/release-notes-generator semantic-release
```

### Create tokens

Create a [gitlab token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) (api, read_user, read_repository, write_repository), an npm token, and generate a new [ssh key pair](https://docs.gitlab.com/ee/ssh/README.html#generating-a-new-ssh-key-pair).

### Add environment variables to repository

```sh
GL_TOKEN
```

```sh
NPM_TOKEN
```

```sh
SSH_PRIVATE_KEY
```

### Run `build docker image` stage

The `build docker image` must be run manually once.