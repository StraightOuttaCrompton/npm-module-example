## [1.0.2](https://gitlab.com/StraightOuttaCrompton/npm-module-example/compare/v1.0.1...v1.0.2) (2020-04-21)


### Bug Fixes

* renamed module to avoid module naming clash ([7c2db3d](https://gitlab.com/StraightOuttaCrompton/npm-module-example/commit/7c2db3daeb2ef00f5299b9bdf7b96e530e74f4d8))
* renamed module to avoid naming clash ([4038d98](https://gitlab.com/StraightOuttaCrompton/npm-module-example/commit/4038d989bdd3cf588102f2d8dc1cc242236ebe95))

## [1.0.1](https://gitlab.com/StraightOuttaCrompton/npm-module-example/compare/v1.0.0...v1.0.1) (2020-04-21)


### Bug Fixes

* added husky precommit hook ([92e5ecd](https://gitlab.com/StraightOuttaCrompton/npm-module-example/commit/92e5ecd25ec3bcb18ef27c3c3a52b58eba2931c3))

# 1.0.0 (2020-04-21)


### Features

* initial commit ([692e851](https://gitlab.com/StraightOuttaCrompton/npm-module-example/commit/692e851e2eec4a2e4514bc6f983b2a62719c3686))
